using System.Collections.Generic;
using UnityEngine;

// generates the layout of the maze using an algorithm
public class MazeDataGenerator
{
    public float placementThreshold;    // chance of empty space

    public MazeDataGenerator()
    {
        placementThreshold = .1f;       // default threshold
    }

    public int[,] FromDimensions(int sizeRows, int sizeCols)
    {
        int[,] maze = new int[sizeRows, sizeCols];
        int rMax = maze.GetUpperBound(0);
        int cMax = maze.GetUpperBound(1);

        for (int i = 0; i <= rMax; i++)
        {
            for (int j = 0; j <= cMax; j++)
            {
                // walls around the edges of the maze
                if (i == 0 || j == 0 || i == rMax || j == cMax)
                {
                    maze[i, j] = 1;
                }

                // only compute for every other cell
                else if (i % 2 == 0 && j % 2 == 0)
                {
                    // use a random value to decide whether to skip the cell for variation
                    if (Random.value > placementThreshold)
                    {
                        // assign as wall
                        maze[i, j] = 1;

                        // randomly assign 0, 1 or -1 to a
                        int a = Random.value < .5 ? 0 : (Random.value < .5 ? -1 : 1);
                        // if a is not 0 then b is 0, otherwise b is either 1 or -1
                        int b = a != 0 ? 0 : (Random.value < .5 ? -1 : 1);
                        // the second wall is always adjacent to the current wall
                        maze[i+a, j+b] = 1;
                    }
                }
            }
        }

        return maze;
    }
}
