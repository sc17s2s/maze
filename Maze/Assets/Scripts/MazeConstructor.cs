using UnityEngine;

public class MazeConstructor : MonoBehaviour
{
    // for debugging and DFS
    public bool showDebug;
    public bool calledDFS;
    public int pathLength;
    private MazeDataGenerator dataGenerator;
    private MazeMeshGenerator meshGenerator;

    [SerializeField] private Material mazeMat1;
    [SerializeField] private Material mazeMat2;
    [SerializeField] private Material startMat;
    [SerializeField] private Material treasureMat;

    // public with private set makes it read only outside this class
    public float hallWidth
    {
        get; private set;
    }
    public float hallHeight
    {
        get; private set;
    }

    public int startRow
    {
        get; private set;
    }
    public int startCol
    {
        get; private set;
    }

    public int goalRow
    {
        get; private set;
    }
    public int goalCol
    {
        get; private set;
    }

    public int[,] data
    {
        get; private set;
    }

    void Awake()
    {
        dataGenerator = new MazeDataGenerator();
        meshGenerator = new MazeMeshGenerator();

        // default to walls surrounding a single empty cell
        data = new int[,]
        {
            {1, 1, 1},
            {1, 0, 1},
            {1, 1, 1}
        };
    }

    public void GenerateNewMaze(int sizeRows, int sizeCols,
        TriggerEventHandler startCallback=null, TriggerEventHandler goalCallback=null)
    {
        if (sizeRows % 2 == 0 && sizeCols % 2 == 0)
        {
            Debug.LogError("Odd numbers work better for dungeon size.");
        }

        DisposeOldMaze();

        calledDFS = false;
        pathLength = 0;

        data = dataGenerator.FromDimensions(sizeRows, sizeCols);

        FindStartPosition();
        FindGoalPosition();

        // store values used to generate this mesh
        hallWidth = meshGenerator.width;
        hallHeight = meshGenerator.height;

        DisplayMaze();

        PlaceStartTrigger(startCallback);
        PlaceGoalTrigger(goalCallback);
    }

    private void DisplayMaze()
    {
        GameObject go = new GameObject();
        go.transform.position = Vector3.zero;
        go.name = "Procedural Maze";
        go.tag = "Generated";

        MeshFilter mf = go.AddComponent<MeshFilter>();
        mf.mesh = meshGenerator.FromData(data);

        MeshCollider mc = go.AddComponent<MeshCollider>();
        mc.sharedMesh = mf.mesh;

        MeshRenderer mr = go.AddComponent<MeshRenderer>();
        mr.materials = new Material[2] {mazeMat1, mazeMat2};
    }

    // destroy the game objects making up the maze
    public void DisposeOldMaze()
    {
        GameObject[] objects = GameObject.FindGameObjectsWithTag("Generated");
        foreach (GameObject go in objects) {
            Destroy(go);
        }
    }

    // find the first empty space to be the start
    private void FindStartPosition()
    {
        int[,] maze = data;
        int rMax = maze.GetUpperBound(0);
        int cMax = maze.GetUpperBound(1);

        for (int i = 0; i <= rMax; i++)
        {
            for (int j = 0; j <= cMax; j++)
            {
                if (maze[i, j] == 0)
                {
                    startRow = i;
                    startCol = j;
                    return;
                }
            }
        }
    }

    // find the last empty space to be the goal
    private void FindGoalPosition()
    {
        int[,] maze = data;
        int rMax = maze.GetUpperBound(0);
        int cMax = maze.GetUpperBound(1);

        // loop top to bottom, right to left
        for (int i = rMax; i >= 0; i--)
        {
            for (int j = cMax; j >= 0; j--)
            {
                if (maze[i, j] == 0)
                {
                    goalRow = i;
                    goalCol = j;
                    return;
                }
            }
        }
    }

    private void PlaceStartTrigger(TriggerEventHandler callback)
    {
        GameObject go = GameObject.CreatePrimitive(PrimitiveType.Cube);
        go.transform.position = new Vector3(startCol * hallWidth, .5f, startRow * hallWidth);
        go.name = "Start Trigger";
        go.tag = "Generated";

        go.GetComponent<BoxCollider>().isTrigger = true;
        go.GetComponent<MeshRenderer>().sharedMaterial = startMat;

        TriggerEventRouter tc = go.AddComponent<TriggerEventRouter>();
        tc.callback = callback;
    }

    private void PlaceGoalTrigger(TriggerEventHandler callback)
    {
        GameObject go = GameObject.CreatePrimitive(PrimitiveType.Cube);
        go.transform.position = new Vector3(goalCol * hallWidth, .5f, goalRow * hallWidth);
        go.name = "Treasure";
        go.tag = "Generated";

        go.GetComponent<BoxCollider>().isTrigger = true;
        go.GetComponent<MeshRenderer>().sharedMaterial = treasureMat;

        TriggerEventRouter tc = go.AddComponent<TriggerEventRouter>();
        tc.callback = callback;
    }

    // shows maze with 0 for free space and 1 for wall
    void OnGUI()
    {
        // does not display anything if debugging is off
        if (!showDebug)
        {
            return;
        }

        int[,] maze = data;
        int rMax = maze.GetUpperBound(0);
        int cMax = maze.GetUpperBound(1);

        string msg = "";

        // start from the top row to build a string to show the maze
        for (int i = rMax; i >= 0; i--)
        {
            for (int j = 0; j <= cMax; j++)
            {
                if (i == startRow && j == startCol)
                {
                    maze[i, j] = 2;
                    msg += "2";
                }
                else if (i == goalRow && j == goalCol)
                {
                    maze[i, j] = 3;
                    msg += "3";
                }
                else if (maze[i, j] == 0)
                {
                    msg += "0";
                }
                else if (maze[i, j] == 1)
                {
                    msg += "1";
                }
                else if (maze[i, j] == 4)
                {
                    msg += "4";
                }
            }
            msg += "\n";
        }

        msg += pathLength.ToString();

        // displays debug info at the side
        GUI.Label(new Rect(20, 200, 500, 500), msg);

        // call DFS to check if a path exists
        if (!calledDFS)
        {
            if (DFS(startRow, startCol, maze))
            {
                Debug.Log("Path exists");
            }
            else
            {
                Debug.Log("No path");
            }
        }

        
    }

    bool DFS(int x, int y, int[,] maze)
    {
        calledDFS = true;
        pathLength++;

        // goal reached
        if (maze[x, y] == 3)
        {
            return true;
        }

        // mark as visited
        maze[x, y] = 4;

        // try right
        if (maze[x+1, y] != 1 && maze[x+1, y] != 4 && DFS(x+1, y, maze))
        {
            return true;            
        }
        // try up
        if (maze[x, y+1] != 1 && maze[x, y+1] != 4 && DFS(x, y+1, maze))
        {
            return true;            
        }
        // try left
        if (maze[x-1, y] != 1 && maze[x-1, y] != 4 && DFS(x-1, y, maze))
        {
            return true;            
        }
        // try down
        if (maze[x, y-1] != 1 && maze[x, y-1] != 4 && DFS(x, y-1, maze))
        {
            return true;            
        }
        
        // if none of the above
        return false;
    }

}
