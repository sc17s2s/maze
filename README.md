# Maze Generation Game

## User Manual

**Running the game executable:** 

1) Download the folder labelled “Maze Game” for your operating system from the Gitlab repository: https://gitlab.com/sc17s2s/maze. 

2) Double click on the executable file to launch the game. 

3) Press the escape key to quit the application. 

 

**Building and running the project on Unity Editor:**

1) Download Unity Hub from https://unity3d.com/get-unity/download. 

2) Follow the instructions to set it up. 

3) In Unity Hub, add the Unity version 2019.3.12f1. 

4) Download the Maze repository on Gitlab: https://gitlab.com/sc17s2s/maze. 

5) Go to Projects in Unity Hub and add the Maze project. 

6) Launch the project which will open the Unity Editor. 

7) Click the play button (triangle near the upper middle of the screen) to build and run the game. 

8) To see the debugging information, click on the Controller GameObject in the Hierarchy window and turn the Show Debug option on in the Inspector window before playing the game. 
